require: patterns.sc
require: numProcessing.js

theme: /

    state: Start
        q!: $regexp</start>
        q!: * $greeting *
        a: Привет, сыграем в игру? \n\nЯ загадываю четырехзначное число, а ты отгадывай. Угадаешь разряд - это "бык". Угадаешь цифру не на своём месте - "корова". Цифры в числах не должны повторяться. \n\nСогласен?
        
        state: Agree
            q: * (да/давай*/согласен/согласна/ага/угу/вперед/вперёд/lf/ок/ok) *
            q: *(еще/ещё/заново/снова)* || fromState = "/Start/Agree/BotNum/EnterSupp/CheckNumber", onlyThisState = true
            go!: BotNum
            
            state: BotNum
                script:
                    $session.botNum = random4()
                a: Я загадал.
                go!: EnterSupp
                
                state: EnterSupp
                    random:
                        a: Вводи свою догадку.
                        a: Угадывай.
                    
                    state: CheckNumber
                        q: * $usNum *
                        script:
                            $session.usNum = $request.query
                        if: $session.usNum.length != 4
                            a: Введенное число не четырехзначное. Попробуй ещё раз.
                        
                        elseif: containsRepetit($session.usNum) != 0
                            a: Цифры в числе не должны повторяться.

                        elseif: $session.botNum == $session.usNum
                            a: Угадал всё число! Невероятно! Ещё раз?
                                
                        else: 
                            script:
                                $session.bull = countBulls ($session.botNum, $session.usNum)
                                $session.cow = countCows ($session.botNum, $session.usNum)
                            if: ($session.bull!=0) | ($session.cow!=0)
                                a: Быков: {{$session.bull}}, коров: {{$session.cow}}. Моё число было {{$session.botNum}}. Сыграем ещё?
                            #a: Поздравляю! {{$session.bull}} {{$caila.conform("Быки", $session.bull}} и {{$session.cow}} {{$caila.conform("коровы", $session.cow}}.
                            #закомментированная реплика сверху вызывает ошибку: RuntimeException: java.util.concurrent.ExecutionException: com.justai.zb.scenarios.errors.ScriptingException: /jsapi/reactions.js:21:16 SyntaxError: <function>:10:3 Expected ) but found ;
                            
                            else: 
                                random:
                                    a: Ни быков, ни коров...
                                    a: Ферма - это не твое.
                                    a: Ни одного совпадения.
                                
                                go!:..
                    
            state: Stop
                q: * (устал*/хватит/все/всё/не могу/стоп/останови*/сдаюсь/выход/выйти/выйди)
                a: Жаль. Моё число было {{$session.botNum}}. До новых встреч.
                            
            state: LocalCatchAll || noContext = true
                event: noMatch
                a: Пожалуйста, введи четырёхзначное число.
        state: Disagree
            q: (нет/не хочу/не буду)
            a: Тогда пока.
            

        
        state: CatchAll || noContext = true
            event!: noMatch
            random:
                a: Я не понял.
                a: Переформулируй.
                
            

        
